import tweepy
from textblob import TextBlob
import pandas as pd
from sensitive import twitter_api
import numpy as np

consumer_key, consumer_key_secret, access_key, access_key_secret = twitter_api()
# provide your credentials here


def tweets(consumer_key, consumer_key_secret, access_key, access_key_secret):
    auth = tweepy.OAuthHandler(consumer_key, consumer_key_secret)
    auth.set_access_token(access_key, access_key_secret)
    api = tweepy.API(auth)
    return api


api = tweets(consumer_key, consumer_key_secret, access_key, access_key_secret)

search = input('Search : ')
public_tweets = api.search(search)

df = pd.DataFrame()

t = []
s = []

for tweet in public_tweets:
    t.append(tweet.text)
    analysis = TextBlob(tweet.text)
    s.append(analysis.sentiment[0])

df['tweets'] = t
df['sentiments'] = s

sumd = np.sum(s)

if sumd < 0:
    print('Overall Negative : ' + str(sumd))
elif sumd > 0:
    print('Overall Positive : ' + str(sumd))
else:
    print('Overall Neutral')
print(df)
